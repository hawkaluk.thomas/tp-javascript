let imc;


function CalculerIMC(prmKilo, prmCm) {
    let valeurIMC;
    valeurIMC = prmKilo / ((prmCm / 100) * (prmCm / 100));
    return valeurIMC;
}

function interpreterIMC(prmIMC) {
    let interpretation = "";
    if(prmIMC<16.5){
        interpretation = "Dénutrition";
    }
    else if(prmIMC<18.5){
        interpretation = "Maigreur";
    }
    else if(prmIMC<25){
        interpretation = "Corpulence normale";
    }
    else if(prmIMC<30){
        interpretation = "Surpoids";
    }
    else if(prmIMC<35){
        interpretation = "Obésité modérée";
    }
    else if(prmIMC<40){
        interpretation = "Obésité sévère";
    }
    else {
        interpretation = "Obésité morbide";
    }
    return interpretation;
}

imc = CalculerIMC(100,175);
console.log("Votre IMC est égal à " + imc.toFixed(1) + " : vous êtes en " + interpreterIMC(imc));
