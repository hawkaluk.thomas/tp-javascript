function calculerTempsParcoursSec(prmVitesse,prmDistance) {
    let valeurderetour = prmDistance/prmVitesse*3600;
    return valeurderetour;
}
function convertiseur(prmtempensec) {
    let heures;
    let minutes;
    let secondes;
    let valeurderetour;
    heures = Math.floor(prmtempensec/3600);
    minutes = Math.floor((prmtempensec/60) - (heures*60));
    secondes = Math.floor(prmtempensec- (minutes*60) - (heures*3600))
    valeurderetour = heures + "h " + minutes + "min " + secondes + "s";
    return valeurderetour;
}
let vitesse = 90;
let distance = 500; 
console.log("Calcul du temps de parcours d'un trajet :");
console.log("Vitesse moyenne (en km/h) :   "+vitesse);
console.log("Distance à parcourir (en km): "+distance);
console.log("A " + vitesse + " km/h, une distance de " + distance + " km est parcourue en " + calculerTempsParcoursSec(vitesse,distance) + " s");
tempsensec = calculerTempsParcoursSec(vitesse,distance);
console.log("A " + vitesse + " km/h, une distance de " + distance + " km est parcourue en " + convertiseur(tempsensec));