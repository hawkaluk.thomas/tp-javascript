function rechercher_Multiple_de_3(prmmax) {
    let valeurderetour = "0";
    for (let i = 1; i < prmmax; i++) {
        if (i%3 == 0){
            valeurderetour = valeurderetour + "-" + i;
        }
    }
    return valeurderetour;
}
console.log("Recherches des multiples de 3 :");
let valeurlimites = 20;
console.log("Valeur limite de la recherche :", valeurlimites);
console.log("Multiples de 3 : " + rechercher_Multiple_de_3(valeurlimites));
