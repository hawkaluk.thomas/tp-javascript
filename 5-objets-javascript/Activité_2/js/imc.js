function patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {
    this.nom = prmNom,
        this.prenom = prmPrenom,
        this.age = prmAge,
        this.sexe = prmSexe,
        this.taille = prmTaille, //cm
        this.poids = prmPoids, //kg
        this.decrire = function () {
            let genre = this.sexe;
            let description = "";
            if ((genre == 'Masculin') || (genre == 'masculin')) {
                description = "le patient dénommé " + this.prenom + " " + this.nom + " est de sexe " + this.sexe + " il est âgé de " + this.age + " ans. il mesure " + this.taille + " cm et pèse " + this.poids + " kg";
            } else if ((genre == 'Feminin') || (genre == 'feminin')) {
                description = "la patiente dénommée " + this.prenom + " " + this.nom + " est de sexe " + this.sexe + " elle est âgée de " + this.age + " ans. elle mesure " + this.taille + " cm et pèse " + this.poids + " kg";
            }

            return description;
        }

    this.definir_corpulence = function (prmObj) {


        function CalculerIMC() {
            let calculIMC = 0;
            calculIMC = prmObj.poids / ((prmObj.taille / 100) * (prmObj.taille / 100));
            return calculIMC;
        }
        let IMC = CalculerIMC().toFixed(2);
        let genre = prmObj.sexe;
        function interpreter_IMC(prmIMC) {
            let interpretation = "";
            if ((genre == 'Masculin') || (genre == 'masculin')) {
                if (prmIMC < 16.5) {
                    interpretation = "Il est en situation" + "denutrition";
                } else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
                    interpretation = "Il est en situation" + "maigreur";
                } else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
                    interpretation = "Il est en situation" + "corpulence normal";
                } else if ((prmIMC >= 25) && (prmIMC < 30)) {
                    interpretation = "Il est en situation" + "surpoids";
                } else if ((prmIMC >= 30) && (prmIMC < 35)) {
                    interpretation = "Il est en situation" + "obésité modéré";
                } else if ((prmIMC >= 35) && (prmIMC <= 40)) {
                    interpretation = "Il est en situation" + "obésité severe";
                } else if (prmIMC > 40) {
                    interpretation = "Il est en situation" + "Obésité morbide";
                }
            } else if ((genre == 'Feminin') || (genre == 'feminin')) {
                if (prmIMC < 14.5) {
                    interpretation = "Elle est en situation" + "denutrition";
                } else if ((prmIMC >= 14.5) && (prmIMC < 16.5)) {
                    interpretation = "Elle est en situation" + "maigreur";
                } else if ((prmIMC >= 16.5) && (prmIMC < 23)) {
                    interpretation = "Elle est en situation" + "corpulence normal";
                } else if ((prmIMC >= 23) && (prmIMC < 28)) {
                    interpretation = "Elle est en situation" + "surpoids";
                } else if ((prmIMC >= 28) && (prmIMC < 33)) {
                    interpretation = "Elle est en situation" + "obésité modéré";
                } else if ((prmIMC >= 33) && (prmIMC <= 38)) {
                    interpretation = "Elle est en situation" + "obésité severe";
                } else if (prmIMC > 38) {
                    interpretation = "Elle est en situation" + "obésité morbide";
                }
            }
            return interpretation;

        }
        let interpret = interpreter_IMC(IMC);
        let affichage = "Son IMC est de: " + IMC + interpret;
        return affichage;
    }



};

let objPatient = new patient('Dupond', 'Jean', 30, 'Masculin', 180, 85);
let objPatient2 = new patient('Moulin', 'Isabelle', 46, 'Feminin', 158, 74);
let objPatient3 = new patient('Martin', 'Eric', 42, 'Masculin', 165, 90);
console.log(objPatient.decrire());
console.log(objPatient.definir_corpulence(objPatient));
console.log(objPatient2.decrire());
console.log(objPatient2.definir_corpulence(objPatient2));
console.log(objPatient3.decrire());
console.log(objPatient3.definir_corpulence(objPatient3));