function Personnage(prmNom, prmNiveau) {
    this.nom = prmNom;
    this.prmNiveau = prmNiveau;
}

Personnage.prototype.saluer = function () {
    let message
    message = this.nom + " vous salue!!";
    return message;
}

function Guerrier(prmNom, prmNiveau, prmArmes) {
    Personnage.call(this, prmNom, prmNiveau);
    this.armes = prmArmes;
}

Guerrier.prototype = Object.create(Personnage.prototype);

Guerrier.prototype.combattre = function () {
    let message;
    message = this.nom + " est un guerrier qui se combat avec une " + this.armes;
    return message;
}

function Magicien(prmNom, prmNiveau, prmPouvoir) {
    Personnage.call(this, prmNom, prmNiveau);
    this.pouvoir = prmPouvoir;
}

Magicien.prototype = Object.create(Personnage.prototype);

Magicien.prototype.posseder = function () {
    let message;
    message = this.nom + " est un magicien qui le pouvoir de " + this.pouvoir;
    return message;
}

let objPersonnage1 = new Guerrier('Arthur', 3, 'épée');
let objPersonnage2 = new Magicien('Merlin', 2, 'prédire les batailles');
console.log(objPersonnage1.saluer());
console.log(objPersonnage1.combattre());
console.log(objPersonnage2.saluer());
console.log(objPersonnage2.posseder());