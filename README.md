# TP javascript

> * Auteur : Thomas HAWKALUK
> * Date de publication : 08/04/2021 


## Sommaire

  - Introduction
    - [HelloWorld](introduction/HelloWorldJS/index.html)
    - Variables et opérateurs
      - [Activité](2-variables-operateurs/Calcul_Surface/index.html)
      - [Exercice 1](2-variables-operateurs/Exercice_1/index.html)
      - [Exercice 2](2-variables-operateurs/Exercice_2/index.html)
    - Structure et Controles
      - [Activité](3-structures-controles/Suite_Syracuse/index.html)
      - [Exercice 1](3-structures-controles/Exo1/index.html)
      - [Exercice 2](3-structures-controles/Exo2/index.html)
      - [Exercice 3](3-structures-controles/Exo3/index.html)
      - [Exercice 4](3-structures-controles/Exo4/index.html)
      - [Exercice 5](3-structures-controles/Exo5/index.html)
    - Fonction avec JavaScript
      - [Activité](4-fonction-javascript/Activité/index.html)
      - [Exercice 1](4-fonction-javascript/Exo1/index.html)
      - [Exercice 2](4-fonction-javascript/Exo2/index.html)
    - Objets avec JavaScript
      - [Activité](5-objets-javascript/Activité/index.html)
      - [Activité 2](5-objets-javascript/Activité_2/index.html)
      - [Activité 3](5-objets-javascript/Activité_3/index.html)
      - [Activité 4](5-objets-javascript/Activité_4/index.html)
      - [Exercice](5-objets-javascript/Exercice/index.html)
    - Tableau avec JavaScript
      - [Activité](6-Tableau-javascript/Calcul_Moyenne/index.html)